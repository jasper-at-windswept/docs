---
title: Fix element-desktop automatic microphone adjustment Linux
---

# Stop ElementDesktop automatic microphone volume adjustment.

This took a while but thank goodness it is finally fixed.

Create this file at `~/.config/wireplumber/main.lua.d/99-stop-microphone-auto-adjust.lua`

::: warning
The name for this file is important as it provides maximum priority to this files logic.
:::

::: info
If this does not work then change `electron` to the `application.process.binary` of `Chromium input` client in `wpctl`. Example `wpctl status` (lets say Chromium input is `57`), `wpctl inspect 57`.
:::

```lua
    table.insert (default_access.rules,{
        matches = {
            {
                { "application.process.binary", "=", "electron" }
            }
        },
        default_permissions = "rx",
    })
```

If you are on NixOS you can also use `home.file` from home-manager to easily write this file.

`home.nix`
```nix
  home = {
    file = {
      ".config/wireplumber/main.lua.d/99-stop-microphone-auto-adjust.lua".text = ''
        table.insert (default_access.rules,{
            matches = {
                {
                    { "application.process.binary", "=", "electron" }
                }
            },
            default_permissions = "rx",
        })
      '';
    };
  };
```
