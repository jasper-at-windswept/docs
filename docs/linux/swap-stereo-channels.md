---
title: Swap Left and Right channels in Wireplumber
---

# Swap left and right stereo channels in wireplumber

Alright sweet and simple, 
*Edit: Wireplumber now uses .conf instead of .lua*

1. `wpctl status`
2. Find your speakers and their `id` then run `wpctl inspect id`, *replace `id` with the number*
3. Copy the `node.name` value.
4. Create the below file at `~/.config/wireplumber/wireplumber.conf.d/alsa-speakers.conf`

```conf
monitor.alsa.rules = [
  {
    matches = [
      {
        node.name = "~alsa_output.usb-ACTIONS_Pebble_V3-00.*"
      }
    ]
    actions = {
      update-props = {
        audio.position = ["FR", "FL"]
      }
    }
  }
]
```

Reboot your system and check that it worked by repeating steps 1-2 and look at `audio.position`.

::: tip
Didn't work? Try `node.nick` or `alsa.card_name` instead of `node.name`
:::

More information [here](https://bbs.archlinux.org/viewtopic.php?id=285115)
