---
title: Add cover image to audio file with FFmpeg Linux
---

# Add cover image to audio file with ffmpeg

Credit to [baeldung/linux](https://www.baeldung.com/linux/terminal-music-add-album-art)

Pretty simple command but it took me a while to find it.

::: info
The below command works for `mp3, m4a, mp4, ogg, wma`
:::

`ffmpeg -i song.mp3 -i image.jpg -map_metadata 0 -map 0 -map 1 -acodec copy song_with_cover.mp3`

For `.flac` audio files use the below command

`ffmpeg -i song.flac -i image.jpg -map_metadata 0 -map 0 -map 1 -acodec copy -disposition:v attached_pic song_with_cover.flac`

