# Self Hosting Supabase

This is a quick guide for self hosting Supabase, of course.
If I'm being honest the official guide is pretty bad so I'm going to try to make this better.

## Prerequisites

- A knowledge of how to use the terminal
- A server with at least 2GB of RAM
- A knowledge of your server's IP address

## Installing Supabase

I assume you can login to your server via SSH and have `docker` and `git` installed.
If not check your distrobution's documentation for how to install them.

```sh
# Change into your home directory
cd ~

# Clone the supabase repo
git clone --depth 1 https://github.com/supabase/supabase ~/temp-supabase

# Make your project directory
mkdir ~/supabase-project

# Copy over the docker stuff
cp -rf ~/temp-supabase/docker/* ~/supabase-project
cp -rf ~/temp-supabase/docker/.env.example ~/supabase-project/.env


# Change into the supabase directory
cd ~/supabase-project

# Download the docker images
docker compose pull
```

Now we need to modify the `.env` file to setup JWT secrets and studio credentials.

Head to [Generate JWT Secrets](https://jwtsecret.com/generate) and generate a 32 or 64 character secret.
Copy the secret into the `JWT_SECRET` field in the `~/supabase-project/.env` file.

Now go to [this link](https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoic2VydmljZV9yb2xlIiwiaXNzIjoic3VwYWJhc2UiLCJpYXQiOjE3MTQ5NzUyMDAsImV4cCI6MTg3Mjc0MTYwMH0.94n1skq8ijf6a1yZU7by1jyH-_Y21MXIpEcmc4m_jW4) and switch to the decoded tab. Change the token under `verify signature` to you JWT token you just generate earlier. Save the output on the left as `SERVICE_ROLE_KEY` in your `.env` file.

Now for the `ANON_KEY` go to [this link](https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlzcyI6InN1cGFiYXNlIiwiaWF0IjoxNzE0OTc1MjAwLCJleHAiOjE4NzI3NDE2MDB9.pYtda94wDevZj92OG6a6ILutRu2AgZB9Wwm885sGNjE) and repeat the step before, replace `your-secret-key-HERE` with the JWT key you generated before, and put the generate string for the value `ANON_KEY` in the `.env`.

Generate 1 more JWT key that is 32 chars for the `POSTGRES_PASSWORD` on [Generate JWT Secrets](https://jwtsecret.com/generate) and put the generated string for the value `POSTGRES_PASSWORD` in the `.env`.

Last thing before we start the docker containers is to setup a username and password for the studio page.

Do this by changing `DASHBOARD_USERNAME` and `DASHBOARD_PASSWORD` in the `.env` file.

Now we can start the docker containers.

```sh
# Start the supabase instance in the background
cd ~/supabase-project
docker compose up -d

# Stop the supabase instance
docker compose down
```

Now you can access the studio at `http://your-server-ip:3000` and login with the username and password you set in the `.env` file.

## Accessing the Database

If your using a ORM like Prisma or Drizzle you will probably want to access the database directly.
To do these use the following URL:

::: info
POSTGRES_DB defaults to `postgres`
:::

```
postgres://postgres:[your-POSTGRES_PASSWORD]@[your-server-ip]:5432/[POSTGRES_DB]
```
