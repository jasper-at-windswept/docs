---
title: Setup Mimetypes on NixOS
---

# XDG MIMETYPES!!!

Another venture of mine that took many hours.

Set the following in `home.nix` or your home-manager configuration file.

::: info
You don't need to make the `.desktop` files, they are automatically found by NixOS. Their name wil almost always be the applications command and then append `.desktop`.
:::

::: tip
Looking for a full list of mimetypes? [Click here](https://www.sitepoint.com/mime-types-complete-list/)
:::

```nix
  xdg = {
    mimeApps = {
      enable = true;
      defaultApplications = {
        "inode/directory" = ["pcmanfm.desktop"]; # Directories
        "text/plain" = ["emacsclient.desktop"]; # Plain text
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = ["onlyoffice-desktopeditors.desktop"]; # .docx
        "application/vnd.openxmlformats-officedocument.presentationml.presentation" = ["onlyoffice-desktopeditors.desktop"]; # .pptx
        "application/pdf" = ["onlyoffice-desktopeditors.desktop"]; # .pdf
        "application/zip" = ["xarchiver.desktop"];
        "text/*" = ["emacsclient.desktop"]; # Any text files
        "video/*" = ["mpv.desktop"]; # Any video files
        "x-scheme-handler/https" = ["firefox.desktop"]; # Links
        "x-scheme-handler/http" = ["firefox.desktop"]; # Links
        "x-scheme-handler/mailto" = ["firefox.desktop"]; # Links
        "image/*" = ["feh.desktop"]; # Images
        "image/png" = ["feh.desktop"];
        "image/jpeg" = ["feh.desktop"];
      };
    };
  };
```
