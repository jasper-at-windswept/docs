---
title: Easily write files with Home-Manager NixOS
---

# Automatically write common files

Home-manager provides the option to write some files at rebuild using the awesome utility `home.file`

Below is an example of this being used to write out the annoying but always neccesary `.ssh/config` file.

```nix
  home = {
    file = {
      ".ssh/config".text = ''
        Host gitlab.com
          IdentityFile ~/.ssh/gitlab

        Host github.com
          IdentityFile ~/.ssh/gitlab
      '';
    };
  };
```
