---
title: Setting up Anime-Game-Launcher on NixOS
---

# Setting up Anime-Game-Launcher

This one is definitley more complicated and requires you to have flakes setup.

In your `flake.nix`:
```nix
  inputs = {
    aagl = {
     url = "github:ezKEa/aagl-gtk-on-nix";
     inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    aagl,
  }
```

Then under `nixosConfigurations`

```nix
    nixosConfigurations = {
      yourSystemName = lib.nixosSystem {
        # ... Your code
        modules = [
          # Your imports and modules
          {
            nix.settings = aagl.nixConfig;
            imports = [ aagl.nixosModules.default ];
            programs.anime-game-launcher.enable = true; # Impact Game
            programs.honkers-railway-launcher.enable = true; # Railway game
            programs.honkers-launcher.enable = true; # 3rd game
          }
        ];
      };
    };
```

::: info
For reasons I can't type the actual game names here but basically pick any of the game you want from the ones the code snippet and just remove the `programs` you don't want.
:::
