---
title: OBS Virtual Camera on NixOS
---

# OBS Virtual Camera

Getting the OBS Virtual Camera is a bit more complex that it should be but oh well.

In `configuration.nix`:

```nix
boot = {
    extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];
    extraModprobeConfig = ''
      options v4l2loopback devices=1 video_nr=1 card_label="OBS Cam" exclusive_caps=1
    '';
};
```

Then just add OBS to your packages list or use home-manager's [OBS program options](https://mipmip.github.io/home-manager-option-search/?query=obs-studio).
