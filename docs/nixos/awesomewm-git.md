---
title: Getting awesome-git on NixOS
---

# Getting `awesome-git`

When I was moving from XMonad to AwesomeWM I found out that the setup I was trying to replicate needed the `git` version of Awesome for using the `ruled` module.

Here is the solution:

::: warning
Make sure to replace `version` with the latest git commit number. You can leave hash empty and when you rebuild the system it will print an error with the correct hash, just input that and rebuild again.
:::

`configuration.nix`
```nix
xserver.windowManager.awesome = {
    enable = true;
    package = pkgs.awesome.overrideAttrs (old: {
        version = "1f7ac8f9c7ab9fff7dd93ab4b29514ff3580efcf";
        src = pkgs.fetchFromGitHub {
            owner = "awesomeWM";
            repo = "awesome";
            rev = version;
            hash = "sha256-D5CTo4FvGk2U3fkDHf/JL5f/O1779i92UcRpPn+lbpw=";
        };
        patches = [];
        postPatch = ''
            patchShebangs tests/examples/_postprocess.lua
        '';
    });
};
```
