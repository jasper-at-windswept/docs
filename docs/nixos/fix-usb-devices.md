---
title: Fix USB devices not working on NixOS
---

# USB Devices not working

By default NTFS is disabled so you won't be able to access quite a few USB storage devices.

The below code snippet from `configuration.nix` should fix this:

::: info
This will not work for absolutely all NTFS devices, some will need to be formatted to be used or you can access them from a computer using another OS.
:::

```nix
boot.supportedFilesystems = [ "ntfs" ];
```
