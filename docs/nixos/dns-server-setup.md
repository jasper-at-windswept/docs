---
title: How to set DNS server on NixOS
---

# How to set DNS Server?

This one can be tricky but basically just add this to `configuration.nix`:

```nix
networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];
```

::: tip
You can put in just one DNS address and it will still work.
:::

If that does not work as expected and you are using NetworkManager for your internet then make sure to add this line.

```nix
networking = {
    networkmanager = {
        dns = "none";
    };
};
```
