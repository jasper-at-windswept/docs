---
title: Specific Linux Kernel version NixOS
---

# Using a specific Linux kernel version

The wiki explains how to do this pretty well but I thought I would expand on it a bit more.

In `configuration.nix`
```nix
# Change pkgs.linux_6_1 to the sub major kernel package version you want. ^Note1
boot.kernelPackages = pkgs.linuxPackagesFor (pkgs.linux_6_1.override {
  argsOverride = rec {
    src = pkgs.fetchurl {
      # Change "v6.x" to the major version number of the kernel version you want, as an example if you want "4.19.60" you would replace "v6.x" with "v4.x"
      url = "mirror://kernel/linux/kernel/v6.x/linux-${version}.tar.xz";
      # Leave the hash empty and when you rebuild it will print the correct hash which you just put in there.
      hash = "";
    };
    version = "6.1.75";
    modDirVersion = "6.1.75";
  };
});
```

::: info Note 1
To see the avaliable sub major package version run the following commands in order.

1. `nix repl`
2. `:l <nixpkgs>`
3. `pkgs.linuxPackages`

Look for the closest package verion to the one you want, if you want `4.19.60` you would use `pkgs.linuxPackages_4_19`.
:::

::: info Note 2
*If you just want to use the latest linux kernel just use `boot.kernelPackages = pkgs.linuxPackages_latest`*
:::
