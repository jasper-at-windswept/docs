---
title: NvChad Setup on NixOS
---


# NvChad Setup

Since NvChad-v2.5 alot of information on how to setup NvChad on NixOS has become outdated, but thankfully one single person on reddit provided a solution which I will quote here.

1. Clone the `NvChad/starter` repository in your NixOS configurations folder.
2. In your `home.nix`. Make sure to change `./nvchad` to the location of the cloned starter repository relative to the postion of your `home.nix` file.
```nix{2}
home.file.".config/nvim" = {
  source = ./nvchad;
  recursive = true;
  force = true;
}
```

3. Make any changes to the starter config and then rebuild your system.

::: warning Note
Everytime you make changes to a specific nvim plugin config you MAY need to remove that plugin folder from `~/.local/share/nvim/lazy/<plugin-name>` or there may be issues with the plugin.
:::
