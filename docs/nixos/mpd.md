---
title: Setup MPD on NixOS
---

# MPD??????

Yes I know i spent about 4 hours tearing my hair out aswell.

But do not fear, I have the solution.

::: info
Replace `${user}` with your username or keep it the same if you have a `user` variable set.
:::

::: tip
The below uses Pipewire, see the NixOS Wiki page for more information if you are using a different audio server.
:::

`configuration.nix`
```nix{15}
  services = {
    mpd = {
      enable = true;
      musicDirectory = "/home/${user}/Music";
      user = "${user}";
      extraConfig = ''
        audio_output {
          type "pipewire"
          name "Pipewire Output"
        }
      '';
    };
  };

  # Important
  systemd = {
    services = {
      mpd.environment = {
        XDG_RUNTIME_DIR = "/run/user/1000";
      };
    };
  };
```

::: tip
Also include `mpdris2` in your packages list and autostart it(`mpDris2 --music-dir=~/Music &`) if you want tools like `Playerctl` to be able to interact with MPD.
:::
