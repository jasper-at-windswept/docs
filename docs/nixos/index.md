# NixOS

In my opinion the best Linux distrobution. Why do I say this?
Well as a wise man once said:

> "It's garbage, but it's the best we have so far" - [IogaMaster](https://www.youtube.com/@IogaMaster)

Otherwise NixOS is amazing for its reproducability and in that its declarativity.

[[toc]]

## Getting Started

If you are new to NixOS and want help installing or setting everything up please take a look at these videos.

- [Beginner Guide](https://www.youtube.com/watch?v=bjTxiFLSNFA)
- [In-Depth Guide (HM/Flakes/Nix)](https://www.youtube.com/watch?v=AGVXJ-TIv3Y)
- [Flakes and Home Manager Guide](https://www.youtube.com/watch?v=a67Sv4Mbxmc)
- [Another Beginner Guide](https://www.youtube.com/watch?v=1ED9b7ERTzI)
- [Flakes Explanation](https://www.youtube.com/watch?v=ylL6CFEw0Ck)

Aswell here are a few NixOS related channels to keep an eye on.

- [IogaMaster](https://www.youtube.com/@IogaMaster)
- [Vimjoyer](https://www.youtube.com/@vimjoyer)

Looking for more? Take a peek at the sidebar where I show solutions to common and also less common NixOS issues!
