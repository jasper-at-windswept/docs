---
title: GTK and QT Theming on NixOS
---

# GTK and QT Themes

I prefer to set these in my home-manager configuration so that is what these options will be for.

::: info
If you are using a theme that is avaliable in a NixOS package then just pass in the package and the name*. Else you can only pass in name, being the name of the folder that is locally avaliable in `~/.themes`
:::

::: tip
The name of the theme can be gotten by running `nix build nixpkgs#capitaine-cursors`_(replace with your package name)_, then enter the `result` directory and browse until you find the directory with the name of the theme you want, copy that exact directory name into the `name` field.
:::

`home.nix`
```nix
  gtk = {
    enable = true;
    iconTheme = {
      name = "oomox-gruvbox-dark";
      package = pkgs.gruvbox-dark-icons-gtk;
    };
    cursorTheme = {
      name = "capitaine-cursors-white";
      package = pkgs.capitaine-cursors;
    };
    theme = {
      name = "Awesthetic-dark";
    };
  };
```

To enable the QT theme that matches the GTK theme just add:

```nix
  qt = {
    enable = true;
    platformTheme = "gtk";
  };
```

And to use XDG popups that corrospond with the GTK theme you can add the XDG portals information to `configuration.nix` like this:

::: info
There should also be a way to add these portal settings to `home.nix` but I haven't used that yet.
:::

```nix
  xdg = {
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal
        xdg-desktop-portal-gtk
      ];
      config.common.default = "*";
    };
  };
```
