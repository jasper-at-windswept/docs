---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "WindSwept Docs"
  text: "Some Linux and Tech documentation I write in my spare time."
  tagline: by jasper-at-windswept
  actions:
    - theme: brand
      text: Let's Dive In!
      link: /welcome
---
