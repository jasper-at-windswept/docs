# Welcome!

Hey you made it, thanks for coming. We have quite the selection here so feel free to help yourself to anything you would like!

And while your at it you might also like to follow me on [GitHub](https://github.com/jasper-at-windswept)?

- [NixOS](/nixos/index)
- [Linux](/linux/index)
- [Adobe Dw/Ps on Linux](/adobe-on-linux)
- [macOS VM](/macos-vm)
