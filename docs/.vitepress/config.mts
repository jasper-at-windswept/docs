import { defineConfig } from 'vitepress';
import { generateSidebar } from 'vitepress-sidebar';

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "WindSwept Docs",
  description: "Some Linux and Tech documentation I write in my spare time.",
  outDir: '../public',
  cleanUrls: true,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Contact', link: 'mailto:jasper@windswept.digital' }
    ],

    sidebar: generateSidebar({
      documentRootPath: 'docs',
      useTitleFromFileHeading: true,
      capitalizeFirst: true,
      collapsed: true,
      collapseDepth: 2,
      useFolderTitleFromIndexFile: true,
      manualSortFileNameByPriority: [ 'welcome.md', 'adobe-on-linux.md', 'macos-vm.md', 'nixos', 'linux' ]
    }),

    // sidebar: [
    //   {
    //     text: 'Topics',
    //     items: [
    //       { 
    //         text: 'NixOS', 
    //         link: '/nixos',
    //         collapsed: false,
    //         items: [
    //           
    //         ]
    //       },
    //       { text: 'Linux', link: '/linux' },
    //       { text: 'Adobe Dw/Ps on Linux', link: '/adobe-on-linux' },
    //       { text: 'macOS VM', link: '/macos-vm' },
    //     ]
    //   }
    // ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/jasper-at-windswept' }
    ]
  }
})
